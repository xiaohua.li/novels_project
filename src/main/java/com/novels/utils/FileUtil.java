package com.novels.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

public class FileUtil {
    
    public static String saveFile(String folderName, MultipartFile file) {

        UUID uuid = UUID.randomUUID();
        String fileName = folderName + uuid + ".jpg";
        System.out.println("file name - " + fileName);
        byte[] bytes;
        try {
            bytes = file.getBytes();
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(fileName)));
            stream.write(bytes);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return uuid + ".jpg";
    }

    public static String targetZoomOut(String sourcePath) {
        return targetZoomOut(sourcePath, 320, 320);
    }

    public static String targetZoomOut(String sourcePath, int x, int y) {
        // 将目标图片缩小成300*300并保存
        File file1 = new File(sourcePath); // 用file1取得图片名字

        String name = file1.getName();
        String folder = file1.getParent();

        try {
            BufferedImage input = ImageIO.read(file1);
            BufferedImage inputbig = new BufferedImage(x, y, BufferedImage.TYPE_INT_BGR);
            Graphics2D g = (Graphics2D) inputbig.getGraphics();
            g.drawImage(input, 0, 0, x, y, null); // 画图
            g.dispose();
            inputbig.flush();

            String fname = name.substring(0, name.lastIndexOf("."));
            System.out.println("fname - " + fname);
            
            fname = String.format("%s/%s_%dx%d.jpg", folder, fname, x, y);
            System.out.println("new fname - " + fname);
            ImageIO.write(inputbig, "jpg", new File(fname)); 

            return fname;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return name;
    }

    public static void main(String[] args) {
        targetZoomOut("D:\\dev\\temp\\images\\ccc32a70-2980-4379-b61c-d1d4f14bda9e.jpg");
    }

}
