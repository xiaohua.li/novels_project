package com.novels.novel.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.novels.utils.FileUtil;

@RestController
@RequestMapping("/api/upload")
public class UploadFileController {
    private static String UPLOADED_FOLDER = " D:/dev/";

    @RequestMapping(value = "/uploadFile", method = { RequestMethod.POST })
    public @ResponseBody String uploadFile(@RequestParam("name") String name, @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " + name + " into " + name + "-uploaded !";
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }

    @RequestMapping(value = "/multi", method = { RequestMethod.POST })
    public ResponseEntity<?> uploadFile(@RequestParam("bookName") String bookName, 
            @RequestParam("bookContent") String bookContent, 
            @RequestParam("originalUrl") String originalUrl, 
            @RequestParam("bookImage") MultipartFile imageFile, 
            @RequestParam("files") MultipartFile[] uploadfiles) {
        
        System.out.println("bookName - " + bookName);
        System.out.println("bookContent - " + bookContent);
        System.out.println("originalUrl - " + originalUrl);
        
        if(imageFile == null || StringUtils.isEmpty(imageFile.getOriginalFilename())) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }
        
        Map map = new HashMap();
        
        String str = FileUtil.saveFile(UPLOADED_FOLDER, imageFile);
        map.put(imageFile.getOriginalFilename(), str);

        String uploadedFileName = Arrays.stream(uploadfiles).map(x -> x.getOriginalFilename()).filter(x -> !StringUtils.isEmpty(x)).collect(Collectors.joining(" , "));

        if (StringUtils.isEmpty(uploadedFileName)) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }
        try {
            for (MultipartFile file : uploadfiles) {
                if (file.isEmpty()) {
                    map.put(file.getOriginalFilename(), null);
                    continue; // next pls
                }
                str = FileUtil.saveFile(UPLOADED_FOLDER, file);
                map.put(imageFile.getOriginalFilename(), str);
                
                System.out.println("file.getOriginalFilename()" + file.getOriginalFilename());
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity("Successfully uploaded - " + uploadedFileName, HttpStatus.OK);
    }

}
