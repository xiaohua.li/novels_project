package com.novels.novel.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.novels.novel.domain.Novel;
import com.novels.novel.service.NovelService;
import com.novels.weixin.bean.WxJsapiSignature;
import com.novels.weixin.services.WxService;

@Controller
public class ViewNovelController {

    @Autowired
    NovelService novelService;

    @Autowired
    WxService wxService;


    @RequestMapping(value = "/view/{id}")
    public String viewNovel(@PathVariable("id") Long id, Model model) {
        Novel novel = novelService.find(id);
        model.addAttribute("novel", novel);
        model.addAttribute("s_code", UUID.randomUUID().toString());
        return "view";
    }

    @RequestMapping(value = "/api/wx", method = RequestMethod.GET)
    @ResponseBody
    public WxJsapiSignature viewNovel(@RequestParam("url") String url) {
        System.out.println("wx url - " + url);
        
        WxJsapiSignature sign = null;
        try {
            sign = wxService.createJsapiSignature(url);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sign;
    }

    @RequestMapping(value = "/redirect/{id}")
    public RedirectView accessOriginalURL(@PathVariable("id") Long id) {
        Novel novel = novelService.find(id);
        RedirectView redirectView = new RedirectView();
        // redirectView.setUrl("http://www.yahoo.com");
        redirectView.setUrl(novel.getOriginalUrl());
        System.out.println("URL: '" + novel.getOriginalUrl() + "'");
        return redirectView;
    }

}
