package com.novels.novel.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.novels.novel.domain.Novel;
import com.novels.novel.service.NovelService;
import com.novels.utils.FileUtil;

@RestController
@RequestMapping("/api/novel")
public class NovelController {

    @Autowired
    NovelService service;

    @Value("${application.novel_image_path}")
    private String folderName = "D:/dev/projects/new_projects/novels_project/src/main/resources/static/images/";

    @RequestMapping(method = RequestMethod.GET)
    public List<Novel> getAllNoveles() {
        return (List<Novel>) service.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Novel addNovel(@RequestBody Novel Novel) {
        return service.update(Novel);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Novel getNovel(@PathVariable Long id) {
        return service.find(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    Novel updateNovel(@PathVariable Long id, @RequestBody Novel updatedNovel) {
        Novel novel = service.find(id);
        novel.setName(updatedNovel.getName());
        novel.setType(updatedNovel.getType());
        novel.setContent(updatedNovel.getContent());

        return service.update(novel);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    Novel deleteNovel(@PathVariable Long id) {
        return service.deleteNovel(id);
    }

    @RequestMapping(value = "/add", method = { RequestMethod.POST })
    public ResponseEntity<?> addNovel(@RequestParam("bookName") String bookName, @RequestParam("bookShortDesc") String shortDesc, @RequestParam("bookContent") String bookContent, @RequestParam("originalUrl") String originalUrl,
            @RequestParam("bookImage") MultipartFile imageFile, @RequestParam("files") MultipartFile[] uploadfiles) {

        Novel novel = new Novel();
        novel.setName(bookName);
        novel.setOriginalUrl(originalUrl);
        novel.setContent(bookContent);
        novel.setShortDesc(shortDesc);
        
        System.out.println("bookName - " + bookName);
        System.out.println("bookContent - " + bookContent);
        System.out.println("bookShortDesc - " + shortDesc);
        System.out.println("originalUrl - " + originalUrl);

        if (imageFile == null || StringUtils.isEmpty(imageFile.getOriginalFilename())) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }

        String str = FileUtil.saveFile(folderName, imageFile);

        String shortImageUrl = FileUtil.targetZoomOut(folderName + str);

        novel.setImageName(str);

        List list = new ArrayList();

        try {
            for (MultipartFile file : uploadfiles) {
                if (file.isEmpty()) {
                    continue; // next pls
                }
                str = FileUtil.saveFile(folderName, file);
                list.add(str);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String filesName = Stream.of(list).map(x -> x.toString()).collect(Collectors.joining(","));
        novel.setFilesName(filesName);

        service.update(novel);

        return new ResponseEntity(novel, HttpStatus.OK);
    }

}
