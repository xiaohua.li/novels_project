package com.novels.novel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.novels.novel.domain.Novel;
import com.novels.novel.domain.NovelRepository;
import com.novels.novel.service.NovelService;

@Service
public class NovelServiceImpl implements NovelService {

    private final NovelRepository repository;

    @Autowired
    public NovelServiceImpl(NovelRepository repository) {
        this.repository = repository;
    }

    @Override
    public Novel addNovel(Novel novel) {
        // TODO Auto-generated method stub
        return repository.save(novel);
    }

    @Override
    public Novel deleteNovel(Long id) {
        Novel Novel = repository.findOne(id);
        repository.delete(Novel);
        return Novel;
    }

    @Override
    public List<Novel> findAll() {
        // TODO Auto-generated method stub
        return (List<Novel>) repository.findAll();
    }

    @Override
    public Novel find(Long id) {
        return repository.findOne(id);
    }

    @Override
    public Novel findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Novel update(Novel novel) {
        repository.save(novel);
        return novel;
    }

}
