package com.novels.novel.service;

import java.util.List;

import com.novels.novel.domain.Novel;

public interface NovelService {

    Novel addNovel(Novel novel);

    Novel deleteNovel(Long id);

    List<Novel> findAll();

    Novel find(Long id);

    Novel findByName(String name);

    Novel update(Novel novel);

}
