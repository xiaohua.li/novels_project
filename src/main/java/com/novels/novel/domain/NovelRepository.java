package com.novels.novel.domain;

import org.springframework.data.repository.CrudRepository;

public interface NovelRepository extends CrudRepository<Novel, Long> {
	Novel findByName(String name);
}
