package com.novels.novel.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.apache.commons.lang3.StringUtils;

@Entity
public class Novel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @Lob  
    @Column(name="content", columnDefinition="TEXT")  
    private String content;
    private String type;
    private String originalUrl;
    
    @Column(name = "short_desc", length = 2000)
    private String shortDesc;
    
    @Column(name = "image_name", length = 200)
    private String imageName;

    @Column(name = "files_name", length = 1000)
    private String filesName;

    public Novel() {
    }

    public Novel(String name) {
        this.name = name;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getShoreImageUrl() {
        if (StringUtils.isNotBlank(this.imageName)) {
            String substringBefore = StringUtils.substringBeforeLast(imageName, ".");
            String substringAfter = StringUtils.substringAfterLast(imageName, ".");
            return substringBefore + "_320x320." + substringAfter; 
        }
        return "";
    }
    
    public List getFileList() {
        List<String> list = new ArrayList<String>();
        if (StringUtils.isNotBlank(filesName)) {
            String[] arr = StringUtils.split(StringUtils.substringBetween(filesName, "[", "]"), ",");
            for(String str: arr) {
                list.add(str.trim());
            }
        }
        return list;
    }

    public String getFilesName() {
        return filesName;
    }

    public void setFilesName(String filesName) {
        this.filesName = filesName;
    }

}
