package com.novels.weixin.services;

import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.novels.weixin.bean.WxConfig;
import com.novels.weixin.bean.WxJsapiSignature;
import com.novels.weixin.utils.HttpClientUtil;
import com.novels.weixin.utils.RandomUtils;
import com.novels.weixin.utils.SHA1;

@Service
public class WxService {

    private WxConfig wxConfig;

    @Autowired
    public void setWxConfig(WxConfig wxConfig) {
        this.wxConfig = wxConfig;
    }

    public String getJsapiTicket() throws Exception {
        return getJsapiTicket(false);
    }

    public String getJsapiTicket(boolean forceRefresh) throws Exception {
        return this.getAccessToken_ticket();
    }

    public WxJsapiSignature createJsapiSignature(String url) throws Exception {

        long timestamp = System.currentTimeMillis() / 1000;
        String noncestr = RandomUtils.getRandomStr();
        String jsapiTicket = getJsapiTicket(false);

        try {
            String signature = SHA1.genWithAmple("jsapi_ticket=" + jsapiTicket, "noncestr=" + noncestr, "timestamp=" + timestamp, "url=" + url);
            WxJsapiSignature jsapiSignature = new WxJsapiSignature();
            jsapiSignature.setAppid(wxConfig.getAppid());
            jsapiSignature.setTimestamp(timestamp);
            jsapiSignature.setNoncestr(noncestr);
            jsapiSignature.setUrl(url);
            jsapiSignature.setSignature(signature);

            System.out.println("jsapiSignature = " + jsapiSignature);
            return jsapiSignature;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String getAccessToken_ticket() throws Exception {
        System.out.println("getAccessToken_ticket ...");

        String access_token = null; // access_token
        String atime = null;// 获取时间
        int a_expires_in = 0;// 有效时间(s)
        String ticket = null;// jsapi_ticket
        String ttime = null;// 得到时间
        int t_expires_in = 0;// 有效时间(s)
        String result = null;

        String access_tokenStr = getAccessToken();
        System.out.println("access_tokenStr =" + access_tokenStr);

        if (access_tokenStr != null && access_tokenStr.indexOf("access_token") != -1) {
            try {
                JSONObject jsonObject = new JSONObject(access_tokenStr);
                access_token = jsonObject.getString("access_token");
                a_expires_in = jsonObject.getInt("expires_in");

                System.out.println("access_token = " + access_token + " - " + a_expires_in);

                if (access_token != null && !access_token.equals("")) {
                    String ticketStr = getJSAPITicket(access_token);
                    System.out.println("ticketStr:" + ticketStr);

                    if (ticketStr != null && ticketStr.indexOf("ticket") != -1) {
                        try {
                            JSONObject jsonObject2 = new JSONObject(ticketStr);
                            ticket = jsonObject2.getString("ticket");
                            t_expires_in = jsonObject2.getInt("expires_in");

                            System.out.println("ticket = " + ticket + " - " + t_expires_in);

                            result = ticket;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public String getAccessToken() throws Exception {
        String url = String.format(wxConfig.getTokenUrl(), wxConfig.getAppid(), wxConfig.getAppSecret());
        return HttpClientUtil.execute(url);
    }

    public String getJSAPITicket(String access_token) throws Exception {

        String url = String.format(wxConfig.getJsapiTicketUrl(), access_token);
        return HttpClientUtil.execute(url);
    }

    public static void main(String[] args) {
        String access_tokenStr = " {\"errcode\":0,\"errmsg\":\"ok\",\"access_token\":\"eOjS4wgrkAjEEku_ZD0Tko8FOR_ZrcrbfjS1N8CXyCK_OCxrxoX9S7uLQ-9WkOM6UBJHoDupUpBj3oeuS5NQJs_9RNaqeJwj0JMjvTy8c647_i9x2Ipvq5CxT_1YBdHOCNnRYEFJga0EFLJ_BxwqYiAF_YXcSQR5N4bS-g-kbSt8JBSB0R4Gsz1cwhehzs0z0xK3t6xqys5EIF3miP0KSwz0ADCsh24jy8d07ZzZXdU\",\"expires_in\":7200}";

        JSONObject jsonObject = new JSONObject(access_tokenStr);
        String access_token = jsonObject.getString("access_token");
        int expires = jsonObject.getInt("expires_in");

        System.out.println("access_token - " + access_token);
        System.out.println("expires_in - " + expires);

    }

}
