package com.novels.weixin.bean;

import java.io.Serializable;

public class WxJsapiSignature implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String appid;

    private String noncestr;

    private long timestamp;

    private String url;

    private String signature;

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("appid = ").append(appid).append(",");
        sb.append("noncestr = ").append(noncestr).append(",");
        sb.append("timestamp = ").append(timestamp).append(",");
        sb.append("url = ").append(url).append(",");
        sb.append("signature = ").append(signature);
        return sb.toString();
    }
}
