package com.novels.weixin.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("weixin")
public class WxConfig {

    private String appid;
    private String appSecret;

    private String tokenUrl;
    private String jsapiTicketUrl;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getJsapiTicketUrl() {
        return jsapiTicketUrl;
    }

    public void setJsapiTicketUrl(String jsapiTicketUrl) {
        this.jsapiTicketUrl = jsapiTicketUrl;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("appid = ").append(appid).append(",");
        sb.append("appSecret = ").append(appSecret).append(",");
        sb.append("tokenUrl = ").append(tokenUrl).append(",");
        sb.append("jsapiTicketUrl = ").append(jsapiTicketUrl);
        return sb.toString();
    }

}
