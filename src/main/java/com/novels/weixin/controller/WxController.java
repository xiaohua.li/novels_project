package com.novels.weixin.controller;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.novels.weixin.bean.WxJsapiSignature;
import com.novels.weixin.services.WxService;

@Controller
public class WxController {

    @Autowired
    WxService service;

    @RequestMapping("/weixin")
    public String welcome(Map<String, Object> model) {
        model.put("message", "welcome to here...");

        String url = "http://www.baidu.com";
        try {
            WxJsapiSignature sign = service.createJsapiSignature(url);
            model.put("sign", sign);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "welcome";
    }

}
