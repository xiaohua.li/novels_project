# How to run this project

## Tools

jdk8 / nginx / MySQL5.5

## Configuration

nginx.conf - add below config:

        location /images/ { 
            root   "D:/dev/temp/";
        } 
        location / {
            root   html;
            index  index.html index.htm;
            proxy_pass      http://127.0.0.1:8080;
        }

## Build package

- mvn clean package

- java -jar novels_project-0.0.1-SNAPSHOT.jar


